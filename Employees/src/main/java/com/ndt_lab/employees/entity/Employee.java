package com.ndt_lab.employees.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "emp_employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private long employeeId;

    @OneToMany(mappedBy = "employeeId", cascade = CascadeType.ALL)
    @Valid
    private List<History> historyList = new ArrayList<>();

    public void addHistory(History history) {
        historyList.add(history);
        history.setEmployeeId(this);
    }

}
