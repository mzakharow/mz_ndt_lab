package com.ndt_lab.employees.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sys_storage_attachments")
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attachment_id")
    private long id;

    @Column(name = "name_file")
    @NotBlank(message = "Поле \"Имя файла\" должно быть заполнено.")
    private String fileName;

    @Column(name = "content_type")
    @NotBlank(message = "Поле \"Тип файла\" должно быть заполнено.")
    private String contentType;

}
