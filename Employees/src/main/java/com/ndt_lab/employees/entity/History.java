package com.ndt_lab.employees.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "emp_histories")
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "history_id")
    private Long historyId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "emp_employee_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Employee employeeId;

    @Column(name = "photo_file_name")
    @NotBlank(message = "Поле \"Имя файла фотографии\" должно быть заполнено.")
    private String photoFileName;

    @Column(name = "first_name")
    @NotBlank(message = "Поле \"Имя\" должно быть заполнено.")
    private String firstName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "last_name")
    @NotBlank(message = "Поле \"Фамилия\" должно быть заполнено.")
    private String lastName;

    @Column(name = "first_name_genitive")
    @NotBlank(message = "Поле \"Имя в родительном падеже\" должно быть заполнено.")
    private String firstNameGenitive;

    @Column(name = "patronymic_genitive")
    private String patronymicGenitive;

    @Column(name = "last_name_genitive")
    @NotBlank(message = "Поле \"Фамилия в родительном падеже\" должно быть заполнено.")
    private String lastNameGenitive;

    @Column(name = "first_name_dative")
    @NotBlank(message = "Поле \"Имя в дательном падеже\" должно быть заполнено.")
    private String firstNameDative;

    @Column(name = "patronymic_dative")
    private String patronymicDative;

    @Column(name = "last_name_dative")
    @NotBlank(message = "Поле \"Фамилия в дательном падеже\" должно быть заполнено.")
    private String lastNameDative;

    @Column(name = "first_name_transliteration")
    @NotBlank(message = "Поле \"Имя на английском языке\" должно быть заполнено.")
    private String firstNameTransliteration;

    @Column(name = "last_name_transliteration")
    @NotBlank(message = "Поле \"Фамилия на английском языке\" должно быть заполнено.")
    private String lastNameTransliteration;

    @Column(name = "date_birth")
    @Past
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateBirth;

    @Column(name = "inn")
    @NotBlank(message = "Поле \"ИНН\" должно быть заполнено.")
    private String inn;

    @Column(name = "gender")
    @NotBlank(message = "Поле \"Пол\" должно быть заполнено.")
    private String gender;

    @Column(name = "position")
    @NotBlank(message = "Поле \"Должность\" должно быть заполнено.")
    private String position;

    @Column(name = "contractual_entity")
    private String contractualEntity;

    @Column(name = "phone_number_1")
    @NotBlank(message = "Поле \"Личный номер телефона\" должно быть заполнено.")
    private String phoneNumber1;

    @Column(name = "phone_number_2")
    private String phoneNumber2;

    @Column(name = "email")
    @Email(message = "Поле \"Электронная почта\" некорректно заполнено.")
    private String email;

    @Column(name = "pass_series")
    private String passSeries;

    @Column(name = "pass_number")
    @NotBlank(message = "Поле \"Номер паспорта\" должно быть заполнено.")
    private String passNumber;

    @Column(name = "pass_date_issue")
    @Past
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate passDateIssue;

    @Column(name = "pass_issued_by")
    @NotBlank(message = "Поле \"Кем выдан\" должно быть заполнено.")
    private String passIssuedBy;

    @Column(name = "educational_grounding")
    private String educationalGrounding;

    @Column(name = "institution_of_learning")
    private String institutionOfLearning;

    @Column(name = "speciality")
    private String speciality;

    @Column(name = "address_postcode")
    private String addressPostcode;

    @Column(name = "address_country")
    private String addressCountry;

    @Column(name = "address_region")
    private String addressRegion;

    @Column(name = "address_settlement")
    private String addressSettlement;

    @Column(name = "address_street")
    private String addressStreet;

    @Column(name = "address_house")
    private String addressHouse;

    @Column(name = "address_block")
    private String addressBlock;

    @Column(name = "address_flat")
    private String addressFlat;

    @Column(name = "ppi_clothing")
    private String ppiClothing;

    @Column(name = "ppi_headdress")
    private String ppiHeaddress;

    @Column(name = "ppi_footwear")
    private String ppiFootwear;

    @Column(name = "ppi_height")
    private String ppiHeight;

    @Column(name = "ppi_pants")
    private String ppiPants;

    @Column(name = "ppi_gloves")
    private String ppiGloves;

    @Column(name = "dl_category")
    private String dlCategory;

    @Column(name = "begin_date")
    @Past
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime beginDate;

    @Column(name = "end_date")
    @Future
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime endDate;

    @Column(name = "navi_create_user")
    @Min(value = 1, message = "Должен быть указан id пользователя который создал сотрудника.")
    private Long naviCreateUser;

    @Column(name = "navi_create_date")
    private LocalDateTime naviCreateDate;

    @Column(name = "navi_update_user")
    private Long naviUpdateUser;

    @Column(name = "navi_update_date")
    private LocalDateTime naviUpdateDate;
}
