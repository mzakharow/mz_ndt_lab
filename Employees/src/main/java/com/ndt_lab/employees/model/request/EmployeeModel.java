package com.ndt_lab.employees.model.request;

import com.ndt_lab.employees.entity.History;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeModel {
    private long employeeId;
    private History history;
}
