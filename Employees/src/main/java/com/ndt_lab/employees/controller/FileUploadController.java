package com.ndt_lab.employees.controller;

import com.ndt_lab.employees.dao.AttachmentRepository;
import com.ndt_lab.employees.dao.EmployeeRepository;
import com.ndt_lab.employees.entity.Attachment;
import com.ndt_lab.employees.entity.Employee;
import com.ndt_lab.employees.exeption_handling.FileIsEmptyException;
import com.ndt_lab.employees.exeption_handling.NoSuchEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
//TODO - Перенести и выполнить в Security (https://howtodoinjava.com/spring5/webmvc/spring-mvc-cors-configuration/)
public class FileUploadController {

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    AttachmentRepository attachmentRepository;

    @PostMapping("/employeePhoto")
    public @ResponseBody
    String handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {

        if (file.isEmpty()) {
            throw new FileIsEmptyException("This file is empty");
        }

        File uploadDir = new File(uploadPath);

        // Проверяем существует такая директория, если нет то создаем
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // Создаем рандомно имя
        String uuidFile = UUID.randomUUID().toString();
        String resultFileName = uuidFile + "-" + file.getOriginalFilename();

        file.transferTo(new File(uploadPath + "/" + resultFileName));

        // Создаем запись в базе данных
        Attachment newAttachment = new Attachment();
        newAttachment.setFileName(resultFileName);
        newAttachment.setContentType(file.getContentType());
        attachmentRepository.save(newAttachment);

        return resultFileName;
    }

}
