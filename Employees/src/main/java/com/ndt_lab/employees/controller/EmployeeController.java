package com.ndt_lab.employees.controller;

import com.ndt_lab.employees.dao.EmployeeRepository;
import com.ndt_lab.employees.entity.Employee;
import com.ndt_lab.employees.exeption_handling.NoSuchEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
//TODO - Перенести и выполнить в Security (https://howtodoinjava.com/spring5/webmvc/spring-mvc-cors-configuration/)
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;


    //  Получаем весь список сотрудников и их актуальные записи emp_history
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        List<Employee> allEmployees = employeeRepository.findAllActualHistoriesOfEmployees();
        if (allEmployees.isEmpty()) {
            throw new NoSuchEmployeeException("Employees is not in Database");
        }
        return allEmployees;
    }

    //    Получаем работника по id и актуальную запись emp_history данного сотрудника
    @GetMapping("/employee/{id}")
    public Employee getEmployee(@PathVariable Long id) {
        Employee employee;
        Optional<Employee> emp = employeeRepository.findAllActualHistoryOfEmployee(id);

        if (emp.isEmpty()) {
            throw new NoSuchEmployeeException("There is not employee with ID = " +
                    id + " in Database");
        }
        employee = emp.get();
        return employee;
    }

    // Создаем сотрудника и актуальную запись в emp_history с информацией о сотруднике
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/employee")
    public Employee addNewEmployee(@Valid @RequestBody  Employee employee) {
        Employee newEmployee = new Employee();
        newEmployee.addHistory(employee.getHistoryList().get(0));

        return employeeRepository.save(newEmployee);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));

        return errors;
    }




    // Обновляем информацию о сотруднике, то есть создаем новую запись в emp_history с новой информацией о сотруднике
    @PutMapping("/employee/{id}")
    Employee updateEmployee(@PathVariable long id, @RequestBody Employee newEmployee) {
        Employee emp = null;
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isEmpty()) {
            throw new NoSuchEmployeeException("There is not employee with ID = " +
                    id + " in Database");
        } else {
            employee.get().addHistory(newEmployee.getHistoryList().get(0));
        }
        return employeeRepository.save(employee.get());

    }
}
