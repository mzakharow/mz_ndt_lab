package com.ndt_lab.employees.dao;


import com.ndt_lab.employees.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    //    Метод с запросом для получения всех сотрудников и их актуальных записей из emp_history
    @Query("SELECT emp, his FROM Employee emp LEFT JOIN FETCH emp.historyList his " +
            "WHERE his.endDate = '2500-01-01 00:00:00.0'")
            List<Employee> findAllActualHistoriesOfEmployees();

    //    Метод с запросом для получения сотрудника по id и их актуальную запись из emp_history
    @Query("SELECT emp, his FROM Employee emp LEFT JOIN FETCH emp.historyList his WHERE emp.employeeId = ?1 \n" +
            "and his.endDate = '2500-01-01 00:00:00.0'")
    Optional<Employee> findAllActualHistoryOfEmployee(Long id);
}
