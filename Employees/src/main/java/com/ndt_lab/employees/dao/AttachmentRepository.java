package com.ndt_lab.employees.dao;

import com.ndt_lab.employees.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
}