package com.ndt_lab.employees.exeption_handling;

public class FileIsEmptyException extends RuntimeException{
    public FileIsEmptyException(String message) {
        super(message);
    }
}
