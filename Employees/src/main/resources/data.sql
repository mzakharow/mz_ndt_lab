INSERT INTO emp_employees DEFAULT
VALUES;
INSERT INTO emp_employees DEFAULT
VALUES;
INSERT INTO emp_employees DEFAULT
VALUES;

INSERT INTO sys_storage_attachments (name_file, content_type)
VALUES ('Сотрудник1.jpg', 'jpg');

INSERT INTO sys_storage_attachments (name_file, content_type)
VALUES ('Сотрудник2.jpg', 'jpg');

INSERT INTO sys_storage_attachments (name_file, content_type)
VALUES ('Сотрудник3.jpg', 'jpg');

INSERT INTO emp_histories (emp_employee_id, photo_file_name, first_name, patronymic, last_name, first_name_genitive,
                           patronymic_genitive, last_name_genitive, first_name_dative, patronymic_dative,
                           last_name_dative, first_name_transliteration, last_name_transliteration, date_birth,
                           inn, gender, position, contractual_entity, phone_number_1, phone_number_2, email,
                           pass_series, pass_number, pass_date_issue, pass_issued_by, educational_grounding,
                           institution_of_learning, address_postcode, address_country, address_region,
                           address_settlement,
                           address_street, address_house, address_block, address_flat, speciality, ppi_clothing,
                           ppi_headdress, ppi_footwear, ppi_height, ppi_pants, ppi_gloves, dl_category,
                           begin_date, end_date, navi_create_user, navi_update_user)
VALUES (1, 'Сотрудник1.jpg', 'Стас', 'Игоревич', 'Чеховских', 'Стаса', 'Игоревича', 'Чеховских', 'Стасу', 'Игоревичу',
        'Чеховских',
        'Stas', 'Chekhovskikh', '1988-11-28', '246385127964', 'муж', 'специалист', '', '89512214161', '89996196798',
        'astax8828@exampale.com', '4579', '249638', '2008-05-20', 'ОВД России по г. Элиста', 'Высшее',
        'Национальный исследовательский Томский политехнический университет', '693000', 'Российская Федерация',
        'Томская область', 'Томск',
        'Вершинина', '33', null, '217', 'Териолог', '50/L', '56', '43', '180',
        '54', 'L', 'B', '2021-05-25 01:20:16.0', '2500-01-01 00:00:00.0', 1, 1);

INSERT INTO emp_histories (emp_employee_id, photo_file_name, first_name, patronymic, last_name, first_name_genitive,
                           patronymic_genitive, last_name_genitive, first_name_dative, patronymic_dative,
                           last_name_dative, first_name_transliteration, last_name_transliteration, date_birth,
                           inn, gender, position, contractual_entity, phone_number_1, phone_number_2, email,
                           pass_series, pass_number, pass_date_issue, pass_issued_by, educational_grounding,
                           institution_of_learning, address_postcode, address_country, address_region,
                           address_settlement,
                           address_street, address_house, address_block, address_flat, speciality, ppi_clothing,
                           ppi_headdress, ppi_footwear, ppi_height, ppi_pants, ppi_gloves, dl_category,
                           begin_date, end_date, navi_create_user, navi_update_user)
VALUES (2, 'Сотрудник2.jpg', 'Дмитрий', 'Иванович', 'Иванов', 'Ивана', 'Ивановича', 'Иванова', 'Ивану', 'Ивановичу',
        'Иванову', 'Ivan',
        'Ivanov', '1990-12-01', '276439781234', 'муж', 'директор', 'Термо+', '89140967713', null, 'ivanov1990@mail.ru ',
        '1975', '264896', '2003-07-12', 'ОУФМС России по г. Сочи', 'Среднее', null, '693000', 'Российская Федерация',
        'Томская область', 'Томск',
        'Вершинина', '33', null, '217', null, '49/L', '52', '44', '175',
        '50', 'M', 'B, C', '2021-05-26 23:00:10.0', '2500-01-01 00:00:00.0', 2, 1);

INSERT INTO emp_histories (emp_employee_id, photo_file_name, first_name, patronymic, last_name, first_name_genitive,
                           patronymic_genitive, last_name_genitive, first_name_dative, patronymic_dative,
                           last_name_dative, first_name_transliteration, last_name_transliteration, date_birth,
                           inn, gender, position, contractual_entity, phone_number_1, phone_number_2, email,
                           pass_series, pass_number, pass_date_issue, pass_issued_by, educational_grounding,
                           institution_of_learning, address_postcode, address_country, address_region,
                           address_settlement,
                           address_street, address_house, address_block, address_flat, speciality, ppi_clothing,
                           ppi_headdress, ppi_footwear, ppi_height, ppi_pants, ppi_gloves, dl_category,
                           begin_date, end_date, navi_create_user, navi_update_user)
VALUES (2, 'Сотрудник2.jpg', 'Дмитрий_UPDATE', 'Иванович', 'Иванов', 'Ивана', 'Ивановича', 'Иванова', 'Ивану',
        'Ивановичу', 'Иванову',
        'Ivan', 'Ivanov', '1990-12-01', '276439781234', 'муж', 'директор', 'Термо+', '89140967713', null,
        'ivanov1990@mail.ru ', '1975', '264896', '2003-07-12', 'ОУФМС России по г. Сочи', 'Среднее', null, '693000',
        'Российская Федерация', 'Томская область', 'Томск',
        'Вершинина', '33', null, '217', null, '49/L',
        '52', '44', '175', '50', 'M', 'B, C', '2021-05-26 23:00:10.0', '2500-01-01 00:00:00.0', 2, 1);

INSERT INTO emp_histories (emp_employee_id, photo_file_name, first_name, patronymic, last_name, first_name_genitive,
                           patronymic_genitive, last_name_genitive, first_name_dative, patronymic_dative,
                           last_name_dative, first_name_transliteration, last_name_transliteration, date_birth,
                           inn, gender, position, contractual_entity, phone_number_1, phone_number_2, email,
                           pass_series, pass_number, pass_date_issue, pass_issued_by, educational_grounding,
                           institution_of_learning, address_postcode, address_country, address_region,
                           address_settlement,
                           address_street, address_house, address_block, address_flat, speciality, ppi_clothing,
                           ppi_headdress, ppi_footwear, ppi_height, ppi_pants, ppi_gloves, dl_category,
                           begin_date, end_date, navi_create_user, navi_update_user)
VALUES (3, 'Сотрудник3.jpg', 'Илья', 'Николаевич', 'Алексеев', 'Илью', 'Николаевича', 'Алексеева', 'Илье',
        'Николаевичу', 'Алексееву',
        'Ilya', 'Alekseev', '1994-11-11', '159348965712', 'муж', 'руководитель работ', null, '89140902390', null,
        'iliy.1994@mail.ru ', '2489', '347123', '2012-09-13', 'Отделом УФМС России по г. Долгопрудный', 'Высшее',
        'Финансовый университет при Правительстве РФ', '693000', 'Российская Федерация', 'Томская область', 'Томск',
        'Вершинина', '33', null, '217', 'Криптозоолог', '51/XL', '54', '41', '178', '52', 'L', null,
        '2021-05-24 23:00:43.0', '2500-01-01 00:00:00.0', 1, 1);
--
-- INSERT INTO emp_histories (emp_employee_id, photo_file_name, first_name, patronymic, last_name, first_name_genitive,
--                            patronymic_genitive, last_name_genitive, first_name_dative, patronymic_dative,
--                            last_name_dative, first_name_transliteration, last_name_transliteration, date_birth,
--                            inn, gender, position, contractual_entity, phone_number_1, phone_number_2, email,
--                            pass_series, pass_number, pass_date_issue, pass_issued_by, educational_grounding,
--                            institution_of_learning, addressPostcode, addressCountry, addressRegion, addressSettlement,
--                            addressStreet, addressHouse, addressBlock, addressFlat, speciality, ppi_clothing,
--                            ppi_headdress, ppi_footwear, ppi_height, ppi_pants, ppi_gloves, dl_category,
--                            begin_date, end_date, navi_create_user, navi_update_user)
-- VALUES (3, 'Илья_delete', 'Николаевич', 'Алексеев', 'Илью', 'Николаевича', 'Алексеева', 'Илье', 'Николаевичу',
--         'Алексееву', 'Ilya', 'Alekseev', '1994-11-11', '159348965712', 'муж', 'руководитель работ', null, '89140902390',
--         null, 'iliy.1994@mail.ru ', '2489', '347123', '2012-09-13', 'Отделом УФМС России по г. Долгопрудный', 'Высшее',
--         'Финансовый университет при Правительстве РФ', 'Криптозоолог', '51/XL', '54', '41', '178', '52', 'L', null,
--         '2021-05-24 23:00:43.0', '2021-05-27 01:07:00.0', 1, 1);

