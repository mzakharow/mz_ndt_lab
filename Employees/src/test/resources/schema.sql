drop table if exists emp_certificates_criminal_record;
drop table if exists emp_histories;
drop table if exists emp_employees;
drop table if exists sys_storage_attachments;


-- ======================= MainSystem ==================================================================================
create table sys_storage_attachments
(

    attachment_id bigserial not null
        constraint sys_attachment_pk
            primary key,
    name_file varchar(255) not null,
    content_type varchar(10) not null
);

comment on table sys_storage_attachments is 'Таблица с данными о загружаемых файлах';
comment on column sys_storage_attachments.name_file is 'Наименование файла';
comment on column sys_storage_attachments.content_type is 'Формат файла';

alter table sys_storage_attachments
    owner to ndt_user;

-- ======================= Employees ===================================================================================
create table emp_employees
(
    employee_id bigserial not null
        constraint emp_employee_pk
            primary key
);

comment on table emp_employees is 'Таблица с основными данными о сотрудниках';

alter table emp_employees
    owner to ndt_user;

create table emp_certificates_criminal_record
(
    certificate_criminal_record_id bigserial                                not null
        constraint emp_certificate_criminal_record_pk
            primary key,
    emp_employee_id               bigint                                   not null
        constraint emp_employees_certificate_criminal_record_fk
            references emp_employees,
    date_issue                     date,
    begin_date                     timestamptz(1) default current_timestamp not null,
    end_date                       date                                     not null,
    navi_user                      date                                     not null,
    navi_date                      timestamp(1)                             not null
);

comment on table emp_certificates_criminal_record is 'Таблица с данными о справках отстутствия судимости';

comment on column emp_certificates_criminal_record.emp_employee_id is 'Ссылка на id emp_employees';

comment on constraint emp_employees_certificate_criminal_record_fk on emp_certificates_criminal_record is 'Внешний ключ на emp_employees';

comment on column emp_certificates_criminal_record.date_issue is 'Дата выдачи справки';

comment on column emp_certificates_criminal_record.begin_date is 'Дата вступления в должность';

comment on column emp_certificates_criminal_record.end_date is 'Дата окончания должности';

comment on column emp_certificates_criminal_record.navi_user is 'id Пользователя который сделал запись';

comment on column emp_certificates_criminal_record.navi_date is 'Время и дата создания записи';

alter table emp_certificates_criminal_record
    owner to ndt_user;

create table emp_histories
(
    history_id                 bigserial    not null
        constraint emp_history_pk
            primary key,
    emp_employee_id            bigint       not null
        constraint emp_employee_fk
            references emp_employees,
    photo_file_name              varchar(150),
    first_name                 varchar(120) not null,
    patronymic                 varchar(120),
    last_name                  varchar(120) not null,
    first_name_genitive        varchar(120) not null,
    patronymic_genitive        varchar(120),
    last_name_genitive         varchar(120) not null,
    first_name_dative          varchar(120) not null,
    patronymic_dative          varchar(120),
    last_name_dative           varchar(120) not null,
    first_name_transliteration varchar(120),
    last_name_transliteration  varchar(120),
    date_birth                 date         not null,
    inn                        varchar(12),
    gender                     varchar(3)   not null,
    position                   varchar(120),
    contractual_entity         varchar(70),
    phone_number_1             varchar(18)  not null,
    phone_number_2             varchar(16),
    email                      varchar(50)  not null,
    pass_series                varchar(5),
    pass_number                varchar(6)   not null,
    pass_date_issue            date         not null,
    pass_issued_by             varchar(100) not null,
    educational_grounding      varchar(100),
    institution_of_learning    varchar(100),
    speciality                 varchar(100),
    ppi_clothing               varchar(10),
    ppi_headdress              varchar(10),
    ppi_footwear               varchar(10),
    ppi_height                 varchar(10),
    ppi_pants                  varchar(10),
    ppi_gloves                 varchar(10),
    dl_category                varchar(20),
    dl_date_issue              date,
    dl_date_expiration         date,
    begin_date                 timestamp(1) not null,
    end_date                   timestamp(1) not null,
    navi_create_user           bigint       not null,
    navi_create_date           timestamp(1) not null,
    navi_update_user           bigint,
    navi_update_date           timestamp(1) not null
);

comment on table emp_histories is 'Таблица с историческими данными о сотрудниках (Данные которые могу измениться)';

comment on column emp_histories.emp_employee_id is 'Ссылка на id emp_employees';

comment on constraint emp_employee_fk on emp_histories is 'Внешний ключ на emp_employees';

comment on column emp_histories.first_name is 'Имя сотрудника';

comment on column emp_histories.patronymic is 'Отчество';

comment on column emp_histories.last_name is 'Фамилия';

comment on column emp_histories.first_name_genitive is 'Имя в родительном падеже';

comment on column emp_histories.patronymic_genitive is 'Отчество в родительном падеже';

comment on column emp_histories.last_name_genitive is 'Фамилия в родительном падеже';

comment on column emp_histories.first_name_dative is 'Имя в дательном падеже';

comment on column emp_histories.patronymic_dative is 'Отчество в дательном падеже';

comment on column emp_histories.last_name_dative is 'Фамилия в дательном падеже';

comment on column emp_histories.first_name_transliteration is 'Имя транслит';

comment on column emp_histories.last_name_transliteration is 'Фамилия транслит';

comment on column emp_histories.date_birth is 'Дата рождения';

comment on column emp_histories.inn is 'ИНН';

comment on column emp_histories.gender is 'Пол';

comment on column emp_histories.position is 'Должность';

comment on column emp_histories.contractual_entity is 'Подрядчик. Если сотрудник из подрядной организации то указывается наименнование';

comment on column emp_histories.phone_number_1 is 'Первый номер телефона ';

comment on column emp_histories.phone_number_2 is 'Второй номер телефона';

comment on column emp_histories.email is 'Адрес электронной почты';

comment on column emp_histories.pass_series is 'Серия паспорта';

comment on column emp_histories.pass_number is 'Номер паспорта';

comment on column emp_histories.pass_date_issue is 'Дата выдачи';

comment on column emp_histories.pass_issued_by is 'Дата окончания срока действия паспорта';

comment on column emp_histories.educational_grounding is 'Уровень образования';

comment on column emp_histories.institution_of_learning is 'Наименование института';

comment on column emp_histories.speciality is 'Специальность';

comment on column emp_histories.ppi_clothing is 'Размер одежды';

comment on column emp_histories.ppi_headdress is 'Размер головного убора';

comment on column emp_histories.ppi_footwear is 'Размер обуви';

comment on column emp_histories.ppi_height is 'Рост сотрудника';

comment on column emp_histories.ppi_pants is 'Размер брюк';

comment on column emp_histories.ppi_gloves is 'Размер перчаток';

comment on column emp_histories.dl_category is 'Категории водительских прав';

comment on column emp_histories.dl_date_issue is 'Дата выдачи водительских';

comment on column emp_histories.dl_date_expiration is 'Дата окончания водительских';

comment on column emp_histories.begin_date is 'Дата начала';

comment on column emp_histories.end_date is 'Дата окончания';

comment on column emp_histories.navi_create_user is 'id Пользователя который сделал запись';

comment on column emp_histories.navi_create_date is 'Время и дата создания записи';

comment on column emp_histories.navi_update_user is 'id Пользователя который обновил запись';

comment on column emp_histories.navi_update_date is 'Время и дата обновления записи';

alter table emp_histories
    owner to ndt_user;

-- ===================== Triggers ======================================================================================

CREATE OR REPLACE FUNCTION inserting_timestamp_on_create_and_update_emp_histories() RETURNS TRIGGER AS
$histories_user_insert$
BEGIN
    NEW.navi_create_date = timezone('utc', now());
    NEW.navi_update_date = timezone('utc', now());
--     NEW.end_date = (SELECT value FROM sys_constants WHERE name = 'END_DATE_ACTUAL_HISTORY'); TODO После слияния с веткой MainSystem вернуть
    NEW.end_date = '2500-01-01 00:00:00.0';

    IF (SELECT count(*) FROM emp_histories WHERE emp_histories.emp_employee_id = NEW.emp_employee_id) > 0 THEN
        UPDATE emp_histories
        SET end_date = timezone('utc', now()) - interval '1 hour'
        WHERE history_id = (SELECT history_id
                            FROM emp_histories
                            WHERE emp_employee_id = NEW.emp_employee_id
                            ORDER BY emp_histories.navi_create_date DESC
                            limit 1);
    END IF;

    RETURN NEW;
END
$histories_user_insert$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS histories_user_insert ON emp_histories;
CREATE TRIGGER histories_user_insert
    BEFORE INSERT
    ON emp_histories
    FOR EACH ROW
EXECUTE PROCEDURE inserting_timestamp_on_create_and_update_emp_histories();