package com.ndt_lab.employees;

import com.ndt_lab.employees.dao.EmployeeRepository;
import com.ndt_lab.employees.entity.Employee;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class EmployeesApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    EmployeeRepository repository;

    @Before
    private List<Employee> listEmployees() {
        return repository.findAllActualHistoriesOfEmployees();
    }

    private String addNewEmployee() {
        return "{\n" +
                "    \"historyList\": [\n" +
                "        {\n" +
                "            \"photoFileName\": \"Сотрудник1.jpg\",\n" +
                "            \"firstName\": \"Илья TESTING ADD NEW \",\n" +
                "            \"patronymic\": \"Николаевич\",\n" +
                "            \"lastName\": \"Алексеев\",\n" +
                "            \"firstNameGenitive\": \"Илью\",\n" +
                "            \"patronymicGenitive\": \"Николаевича\",\n" +
                "            \"lastNameGenitive\": \"Алексеева\",\n" +
                "            \"firstNameDative\": \"Илье\",\n" +
                "            \"patronymicDative\": \"Николаевичу\",\n" +
                "            \"lastNameDative\": \"Алексееву\",\n" +
                "            \"firstNameTransliteration\": \"Ilya\",\n" +
                "            \"lastNameTransliteration\": \"Alekseev\",\n" +
                "            \"dateBirth\": \"1994-11-11\",\n" +
                "            \"inn\": \"159348965712\",\n" +
                "            \"gender\": \"муж\",\n" +
                "            \"position\": \"руководитель работ\",\n" +
                "            \"contractualEntity\": null,\n" +
                "            \"phoneNumber1\": \"89140902390\",\n" +
                "            \"phoneNumber2\": null,\n" +
                "            \"email\": \"iliy1994@mail.ru\",\n" +
                "            \"passSeries\": \"2489\",\n" +
                "            \"passNumber\": \"347123\",\n" +
                "            \"passDateIssue\": \"2012-09-13\",\n" +
                "            \"passIssuedBy\": \"Отделом УФМС России по г. Долгопрудный\",\n" +
                "            \"educationalGrounding\": \"Высшее\",\n" +
                "            \"institutionOfLearning\": \"Финансовый университет при Правительстве РФ\",\n" +
                "            \"speciality\": \"Криптозоолог\",\n" +
                "            \"ppiClothing\": \"51/XL\",\n" +
                "            \"ppiHeaddress\": 54,\n" +
                "            \"ppiFootwear\": 41,\n" +
                "            \"ppiHeight\": 178,\n" +
                "            \"ppiPants\": \"52\",\n" +
                "            \"ppiGloves\": \"L\",\n" +
                "            \"dlCategory\": null,\n" +
                "            \"beginDate\": \"2021-05-24T23:00:43\",\n" +
                "            \"naviCreateUser\": 1,\n" +
                "            \"naviUpdateUser\": 1\n" +
                "        }\n" +
                "    ]\n" +
                "}";
    }

    private String updateEmployee() {
        return "{\n" +
                "    \"historyList\": [\n" +
                "        {\n" +
                "            \"photoFileName\": \"Сотрудник1.jpg\",\n" +
                "            \"firstName\": \"Илья TESTING UPDATE \",\n" +
                "            \"patronymic\": \"Николаевич\",\n" +
                "            \"lastName\": \"Алексеев\",\n" +
                "            \"firstNameGenitive\": \"Илью\",\n" +
                "            \"patronymicGenitive\": \"Николаевича\",\n" +
                "            \"lastNameGenitive\": \"Алексеева\",\n" +
                "            \"firstNameDative\": \"Илье\",\n" +
                "            \"patronymicDative\": \"Николаевичу\",\n" +
                "            \"lastNameDative\": \"Алексееву\",\n" +
                "            \"firstNameTransliteration\": \"Ilya\",\n" +
                "            \"lastNameTransliteration\": \"Alekseev\",\n" +
                "            \"dateBirth\": \"1994-11-11\",\n" +
                "            \"inn\": \"159348965712\",\n" +
                "            \"gender\": \"муж\",\n" +
                "            \"position\": \"руководитель работ\",\n" +
                "            \"contractualEntity\": null,\n" +
                "            \"phoneNumber1\": \"89140902390\",\n" +
                "            \"phoneNumber2\": null,\n" +
                "            \"email\": \"iliy1994@mail.ru\",\n" +
                "            \"passSeries\": \"2489\",\n" +
                "            \"passNumber\": \"347123\",\n" +
                "            \"passDateIssue\": \"2012-09-13\",\n" +
                "            \"passIssuedBy\": \"Отделом УФМС России по г. Долгопрудный\",\n" +
                "            \"educationalGrounding\": \"Высшее\",\n" +
                "            \"institutionOfLearning\": \"Финансовый университет при Правительстве РФ\",\n" +
                "            \"speciality\": \"Криптозоолог\",\n" +
                "            \"ppiClothing\": \"51/XL\",\n" +
                "            \"ppiHeaddress\": 54,\n" +
                "            \"ppiFootwear\": 41,\n" +
                "            \"ppiHeight\": 178,\n" +
                "            \"ppiPants\": \"52\",\n" +
                "            \"ppiGloves\": \"L\",\n" +
                "            \"dlCategory\": null,\n" +
                "            \"beginDate\": \"2021-05-24T23:00:43\",\n" +
                "            \"naviCreateUser\": 1,\n" +
                "            \"naviUpdateUser\": 1\n" +
                "        }\n" +
                "    ]\n" +
                "}";
    }

    //Ответ getAllEmployees для страницы "Реестр сотрудников" должен содержать для каждого employee_id одну актуальную запись history_id
    //Ответ getAllEmployees для страницы "Реестр сотрудников" должен быть определенной структуры Json.
    @Test
    public void getAllEmployeesApi() throws Exception {

        ResultActions actions = mockMvc
                .perform(get("/api/employees")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        for (int i = 0; i < listEmployees().size(); i++) {
            actions = actions
                    .andExpect(jsonPath("$[" + i + "]").exists())
                    .andExpect(jsonPath("$[" + i + "].historyList[*]", hasSize(1)))
                    .andExpect(jsonPath("$[*].employeeId").exists())
                    .andExpect(jsonPath("$[*].historyList").exists())
                    .andExpect(jsonPath("$[*].historyList[*].historyId").exists())
                    .andExpect(jsonPath("$[*].historyList[*].photoFileName").exists())
                    .andExpect(jsonPath("$[*].historyList[*].firstName").exists())
                    .andExpect(jsonPath("$[*].historyList[*].patronymic").exists())
                    .andExpect(jsonPath("$[*].historyList[*].lastName").exists())
                    .andExpect(jsonPath("$[*].historyList[*].position").exists())
                    .andExpect(jsonPath("$[*].historyList[*].contractualEntity").exists())
                    .andExpect(jsonPath("$[*].historyList[*].phoneNumber1").exists())
                    .andExpect(jsonPath("$[*].historyList[*].phoneNumber2").exists())
                    .andExpect(jsonPath("$[*].historyList[*].email").exists())
                    .andExpect(jsonPath("$[*].historyList[*].dlCategory").exists());
        }
    }

    //Ответ getEmployee для страницы "Сотрудник" должен содержать актуальную запись history_id, запись должна быть одна
    //Ответ getEmployee для страницы "Сотрудник" должен быть определенной структуры Json
    @Test
        public void getEmployeeApi() throws Exception {

        for (int i = 0; i < listEmployees().size(); i++) {
            this.mockMvc
                    .perform(get("/api/employee/{id}", listEmployees().get(i).getEmployeeId())
                            .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.historyList[*]", hasSize(1)))
                    .andExpect(jsonPath("$.employeeId").exists())
                    .andExpect(jsonPath("$.historyList").exists())
                    .andExpect(jsonPath("$.historyList[*].historyId").exists())
                    .andExpect(jsonPath("$.historyList[*].photoFileName").exists())
                    .andExpect(jsonPath("$.historyList[*].firstName").exists())
                    .andExpect(jsonPath("$.historyList[*].patronymic").exists())
                    .andExpect(jsonPath("$.historyList[*].lastName").exists())
                    .andExpect(jsonPath("$.historyList[*].position").exists())
                    .andExpect(jsonPath("$.historyList[*].contractualEntity").exists())
                    .andExpect(jsonPath("$.historyList[*].phoneNumber1").exists())
                    .andExpect(jsonPath("$.historyList[*].phoneNumber2").exists())
                    .andExpect(jsonPath("$.historyList[*].email").exists())
                    .andExpect(jsonPath("$.historyList[*].dlCategory").exists());
        }
    }

    @Test
    public void addNewEmployeeAPI() throws Exception
    {
        this.mockMvc
        .perform(post("/api/employee")
                .content(addNewEmployee())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.employeeId").exists());
    }

    // Обновление актуальной исторической записи сотрудника, проверка статуса ответа??? TODO Стоит это проверять
    @Test
    public void putEmployeeApi() throws Exception {
        this.mockMvc
                .perform(put("/api/employee/{id}", 3)
                        .content(updateEmployee())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
//                .andExpect(jsonPath("$.historyList[*]", hasSize(1)));
    }


}
