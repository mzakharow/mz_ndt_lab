create table sys_system
(
    system_id         bigserial    not null
        constraint sys_system_pk
        primary key,
    name       varchar(120) not null,
    created_at timestamp(6) not null,
    updated_at timestamp(6) not null
);

comment on table sys_system is 'Таблица с данными о системах лабораторий';

comment on column sys_system.name is 'Наименование системы лаборатории';

alter table sys_system
    owner to ndt_user;

create table emp_employees
(
    employees_id            bigserial not null
        constraint emp_employees_pk
        primary key,
    sys_system_id bigint    not null
        constraint emp_employees_system_fk
        references sys_system
);

comment on table emp_employees is 'Таблица с основными данными о сотрудниках';

comment on column emp_employees.sys_system_id is 'Ссылка на id  системы';

comment on constraint emp_employees_system_fk on emp_employees is 'Внешний ключ на sys_system';

alter table emp_employees
    owner to ndt_user;

create table emp_experience
(
    experience_id           bigserial    not null
        constraint emp_experience_pk
        primary key,
    employees_id bigint       not null
        constraint emp_experience_employee_fk
        references emp_employees,
    name         varchar(70) not null,
    begin_date   date         not null,
    end_date     date         not null,
    navi_user    date         not null,
    navi_date    timestamp    not null
);

comment on table emp_experience is 'Таблица с данными о стаже сотрудников';

comment on constraint emp_experience_employee_fk on emp_experience is 'Внешний ключ на emp_employees';

comment on column emp_experience.name is 'Название занимаемой должности';

comment on column emp_experience.begin_date is 'Дата вступления в должность';

comment on column emp_experience.end_date is 'Дата окончания должности';

comment on column emp_experience.navi_user is 'id Пользователя который сделал запись';

comment on column emp_experience.navi_date is 'Время и дата создания записи';

alter table emp_experience
    owner to ndt_user;

create table emp_certificate_criminal_record
(
    certificate_criminal_record_id               bigserial not null
        constraint emp_certificate_criminal_record_pk
        primary key,
    emp_employees_id bigint    not null
        constraint emp_employees_certificate_criminal_record_fk
        references emp_employees,
    date_issue       date,
    begin_date       date      not null,
    end_date         date      not null,
    navi_user        date      not null,
    navi_date        timestamp not null
);

comment on table emp_certificate_criminal_record is 'Таблица с данными о справках отстутствия судимости';

comment on column emp_certificate_criminal_record.emp_employees_id is 'Ссылка на id emp_employees';

comment on constraint emp_employees_certificate_criminal_record_fk on emp_certificate_criminal_record is 'Внешний ключ на emp_employees';

comment on column emp_certificate_criminal_record.date_issue is 'Дата выдачи справки';

comment on column emp_certificate_criminal_record.begin_date is 'Дата вступления в должность';

comment on column emp_certificate_criminal_record.end_date is 'Дата окончания должности';

comment on column emp_certificate_criminal_record.navi_user is 'id Пользователя который сделал запись';

comment on column emp_certificate_criminal_record.navi_date is 'Время и дата создания записи';

alter table emp_certificate_criminal_record
    owner to ndt_user;

create table emp_employees_history
(
    employees_history_id                         bigserial    not null
        constraint emp_employees_history_pk
        primary key,
    emp_employees_id           bigint       not null
        constraint emp_employees_history_fk
        references emp_employees,
    first_name                 varchar(120) not null,
    patronymic                 varchar(120),
    last_name                  varchar(120) not null,
    first_name_genitive        varchar(120) not null,
    patronymic_genitive        varchar(120),
    last_name_genitive         varchar(120) not null,
    first_name_dative          varchar(120) not null,
    patronymic_dative          varchar(120),
    last_name_dative           varchar(120) not null,
    first_name_transliteration varchar(120),
    last_name_transliteration  varchar(120),
    date_birth                 date         not null,
    inn                        varchar(12),
    gender                     varchar(3) not null,
    contractual_entity         varchar(70),
    phone_number_1             varchar(15)         not null,
    phone_number_2             varchar(15),
    email                      varchar(50)        not null,
    pass_series                varchar(5),
    pass_number                varchar(6)   not null,
    pass_date_issue            date         not null,
    pass_issued_by             varchar(100) not null,
    educational_grounding      varchar(100),
    institution_of_learning    varchar(100),
    speciality                 varchar(100),
    ppi_clothing               varchar(6),
    ppi_headdress              varchar(3),
    ppi_footwear               varchar(3),
    ppi_height                 varchar(3),
    ppi_pants                  varchar(7),
    ppi_gloves                 varchar(3),
    dl_category                varchar(20),
    dl_date_issue              date,
    dl_date_expiration         date,
    begin_date                 timestamp    not null,
    end_date                   timestamp    not null,
    navi_create_user           bigint       not null,
    navi_create_date           timestamp    not null,
    navi_update_user           bigint       not null,
    navi_update_date           timestamp    not null
);

comment on table emp_employees_history is 'Таблица с историческими данными о сотрудниках (Данные которые могу измениться)';

comment on column emp_employees_history.emp_employees_id is 'Ссылка на id emp_employees';

comment on constraint emp_employees_history_fk on emp_employees_history is 'Внешний ключ на emp_employees';

comment on column emp_employees_history.first_name is 'Имя сотрудника';

comment on column emp_employees_history.patronymic is 'Отчество';

comment on column emp_employees_history.last_name is 'Фамилия';

comment on column emp_employees_history.first_name_genitive is 'Имя в родительном падеже';

comment on column emp_employees_history.patronymic_genitive is 'Отчество в родительном падеже';

comment on column emp_employees_history.last_name_genitive is 'Фамилия в родительном падеже';

comment on column emp_employees_history.first_name_dative is 'Имя в дательном падеже';

comment on column emp_employees_history.patronymic_dative is 'Отчество в дательном падеже';

comment on column emp_employees_history.last_name_dative is 'Фамилия в дательном падеже';

comment on column emp_employees_history.first_name_transliteration is 'Имя транслит';

comment on column emp_employees_history.last_name_transliteration is 'Фамилия транслит';

comment on column emp_employees_history.date_birth is 'Дата рождения';

comment on column emp_employees_history.inn is 'ИНН';

comment on column emp_employees_history.gender is 'Пол';

comment on column emp_employees_history.contractual_entity is 'Подрядчик. Если сотрудник из подрядной организации то указывается наименнование';

comment on column emp_employees_history.phone_number_1 is 'Первый номер телефона ';

comment on column emp_employees_history.phone_number_2 is 'Второй номер телефона';

comment on column emp_employees_history.email is 'Адрес электронной почты';

comment on column emp_employees_history.pass_series is 'Серия паспорта';

comment on column emp_employees_history.pass_number is 'Номер паспорта';

comment on column emp_employees_history.pass_date_issue is 'Дата выдачи';

comment on column emp_employees_history.pass_issued_by is 'Дата окончания срока действия паспорта';

comment on column emp_employees_history.educational_grounding is 'Уровень образования';

comment on column emp_employees_history.institution_of_learning is 'Наименование института';

comment on column emp_employees_history.speciality is 'Специальность';

comment on column emp_employees_history.ppi_clothing is 'Размер одежды';

comment on column emp_employees_history.ppi_headdress is 'Размер головного убора';

comment on column emp_employees_history.ppi_footwear is 'Размер обуви';

comment on column emp_employees_history.ppi_height is 'Рост сотрудника';

comment on column emp_employees_history.ppi_pants is 'Размер брюк';

comment on column emp_employees_history.ppi_gloves is 'Размер перчаток';

comment on column emp_employees_history.dl_category is 'Категории водительских прав';

comment on column emp_employees_history.dl_date_issue is 'Дата выдачи водительских';

comment on column emp_employees_history.dl_date_expiration is 'Дата окончания водительских';

comment on column emp_employees_history.begin_date is 'Дата начала';

comment on column emp_employees_history.end_date is 'Дата окончания';

comment on column emp_employees_history.navi_create_user is 'id Пользователя который сделал запись';

comment on column emp_employees_history.navi_create_date is 'Время и дата создания записи';

comment on column emp_employees_history.navi_update_user is 'id Пользователя который обновил запись';

comment on column emp_employees_history.navi_update_date is 'Время и дата обновления записи';

alter table emp_employees_history
    owner to ndt_user;

